<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace'=>'User'], function(){
    Route::get('/','PageController@home');
    Route::get('/blog','PageController@blog');
    Route::get('/blog/{slug}','PageController@blogDetail')->name('userblog.detail');
    Route::get('/project','PageController@project');
    Route::get('/project/{slug}','PageController@projectDetail')->name('userproject.detail');
    Route::get('/about-us','PageController@about');
    Route::get('/contact-us','PageController@contact');
});

Route::group(['prefix'=>'admin','namespace'=>'Admin'], function(){
    Route::get('login','AuthController@loginForm');
    Route::get('dashboard','DashboardController@index')->name('admin.dashboard');

    // project
    Route::get('project','ProjectController@index')->name('project.index');
    Route::get('project/create','ProjectController@create')->name('project.create');
    Route::post('project/store','ProjectController@store');
    Route::get('project/show/{id}','ProjectController@show')->name('project.show');
    Route::get('project/edit/{id}','ProjectController@edit')->name('project.edit');
    Route::post('project/update','ProjectController@update');
    Route::delete('project/delete','ProjectController@destroy')->name('project.delete');

    // blog
    Route::get('blog','BlogController@index')->name('blog.index');
    Route::get('blog/create','BlogController@create')->name('blog.create');
    Route::post('blog/store','BlogController@store');
    Route::get('blog/show/{id}','BlogController@show')->name('blog.show');
    Route::get('blog/edit/{id}','BlogController@edit')->name('blog.edit');
    Route::post('blog/update','BlogController@update');
    Route::delete('blog/delete','ProjectController@destroy')->name('blog.delete');
});