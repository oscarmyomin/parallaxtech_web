<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Blog;
use App\Models\Project;

class PageController extends Controller
{
    public function home(){
        return Inertia::render('User/Home');
    }

    public function about(){
        return Inertia::render('User/About');
    }

    public function blog(){
        $blog = Blog::orderBy('id','desc')->get();
        return Inertia::render('User/Blog/Index',['blog'=>$blog]);
    }

    public function blogDetail($slug){
        $blog = Blog::where('slug',$slug)->first();
        return Inertia::render('User/Blog/Show',['blog'=>$blog]);
    }

    public function project(){
        $project = Project::orderBy('id','desc')->get();
        return Inertia::render('User/Project/Index',['project'=>$project]);
    }

    public function projectDetail($slug){
        $project = Project::where('slug',$slug)->first();
        return Insertia::render('User/Project/Show',['project'=>$project]);
    }
    
    public function contact(){
        return Inertia::render('User/Contact');
    }
    
}
