<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Inertia\Inertia;
use App\Models\Blog;

class BlogController extends Controller
{

    public function index()
    {
        $blog = Blog::orderBy('id','desc')->get();
        return Inertia::render('Admin/Blog/Index',['blog'=>$blog]);
    }

    public function create()
    {
        return Inertia::render('Admin/Blog/Create');
    }

    public function store(Request $request)
    {
        $data = new Blog;
        $data->name = $request->name;
        $data->slug = Str::slug($request->name, '-').'_'.time();
        $data->description = $request->description;
        if($request->file('image')){
            $image = $request->file('image');
            $image_name = uniqid().str_replace(' ','-',$image->getClientOriginalName());
            $image_path = '/images/blog/';
            $image->move(public_path($image_path), $image_name);
            $data->image = $image_path.$image_name;
        }
        if($data->save()){
            return redirect()->route('blog.index')->with('success','Created Successfully');
        }else{
            return redirect()->route('blog.index')->with('error','Created Unsuccessfully');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $blog = Blog::findOrFail($id);
        return Inertia::render('Admin/Blog/Edit',['blog'=>$blog]);
    }

    public function update(Request $request, $id)
    {
        $data = Blog::findOrFail($request->id);
        $data->name = $request->name;
        $data->slug = Str::slug($request->name, '-').'_'.time();
        $data->description = $request->description;
        if($request->file('image')){
            $image = $request->file('image');
            $image_name = uniqid().str_replace(' ','-',$image->getClientOriginalName());
            $image_path = '/images/blog/';
            $image->move(public_path($image_path), $image_name);
            $data->image = $image_path.$image_name;
        }
        if($data->update()){
            return redirect()->route('blog.index')->with('success','Created Successfully');
        }else{
            return redirect()->route('blog.index')->with('error','Created Unsuccessfully');
        }

    }

    public function destroy($id)
    {
        $data = Blog::findOrFail($id);
    }
}
