<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Inertia\Inertia;
use App\Models\Project;

class ProjectController extends Controller
{
    public function index()
    {
        $project = Project::orderBy('id','desc')->get();
        return Inertia::render('Admin/Project/Index',['project'=>$project]);
    }

    public function create()
    {
        return Inertia::render('Admin/Project/Create');
    }

    public function store(Request $request)
    {
        $data = new Project;
        $data->name = $request->name;
        $data->slug =  Str::slug($request->name, '-').'_'.time();
        $data->description = $request->description;
        $data->technology = $request->technology;
        $data->link = $request->link;
        $data->type = $request->type;
        $data->is_private = $request->is_private;
        if($request->file('image')){
            $image = $request->file('image');
            $image_name = uniqid().str_replace(' ','-',$image->getClientOriginalName());
            $image_path = '/images/project/';
            $image->move(public_path($image_path), $image_name);
            $data->image = $image_path.$image_name;
        }
        if($data->save()){
            return redirect()->route('project.index')->with('success','Created Successfully');
        }else{
            return redirect()->route('project.index')->with('error','Created Unsuccessfully');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $project = Project::findOrFail($id);
        return Inertia::render('Admin/Project/Edit',['project'=>$project]);
    }

    public function update(Request $request)
    {
        //dd($request->all());
        $data = Project::findOrFail($request->id);
        $data->name = $request->name;
        $data->slug =  Str::slug($request->name, '-').'_'.time();
        $data->description = $request->description;
        $data->technology = $request->technology;
        $data->link = $request->link;
        $data->type = $request->type;
        $data->is_private = $request->is_private;
        if($request->file('image')){
            $image = $request->file('image');
            $image_name = uniqid().str_replace(' ','-',$image->getClientOriginalName());
            $image_path = '/images/project/';
            $image->move(public_path($image_path), $image_name);
            $data->image = $image_path.$image_name;
        }
        if($data->update()){
            return redirect()->route('project.index')->with('success','Created Successfully');
        }else{
            return redirect()->route('project.index')->with('error','Created Unsuccessfully');
        }
    }

    public function destroy($id)
    {
        //
    }
}
