<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Parallax Tech</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="theme-color" content="#203864">
    <meta name="description" content="သင်တို့၏ စီးပွားရေးလုပ်ငန်းများကို ကျွန်ုပ်တို့၏ ဝန်ဆောင်မှုဖြင့် ပိုမိုတိုးတက်အောင် အခုပဲ ဆက်သွယ်လိုက်ပါ။" />
    <meta name="keywords" content="parallax tech, parallax, tech, web, website, development, android, ui, ux, online course, online class" />
    <meta name="author" content="Parallax Tech" />

    <link href="{{ mix('/css/app.css') }}" rel="stylesheet" />
    <script src="{{ mix('/js/app.js') }}" defer></script>
    <script src="https://cdn.tiny.cloud/1/kbeqdtu23r9sshnq6scfomsl9ayy1t25viixa4w3nyjqeckc/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>


    <style>
      inertia-link {
        text-decoration: none;
        color: inherit;
      }

      
    </style>
  </head>
  <body>
    @inertia
    
    @routes

    <script>
      tinymce.init({
          selector: 'textarea#editor',
          plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
          toolbar_mode: 'floating',
      });
  </script>
  </body>
</html>