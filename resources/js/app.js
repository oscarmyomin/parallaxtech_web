import 'alpinejs'
import { InertiaApp } from '@inertiajs/inertia-vue'
import Vue from 'vue'
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css';


Vue.use(InertiaApp)
Vue.use(ElementUI, { locale })

Vue.prototype.$route = (...args) => route(...args).url()

const app = document.getElementById('app')

new Vue({
  render: h => h(InertiaApp, {
    props: {
      initialPage: JSON.parse(app.dataset.page),
      resolveComponent: name => require(`./Pages/${name}`).default,
    },
  }),
}).$mount(app)